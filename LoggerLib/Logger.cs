﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace LoggerLib
{
    public class Logger
    {
        private static Logger _instance { get; set; }
        private Thread _thread;
        private string LogFileName { get; set; }
        private string Path { get; set; }
        private string DateFirmat { get; set; }
        private bool IsEnable { get; set; }
        private bool ToConsole { get; set; }
        private ConcurrentQueue<Log> Logs = new ConcurrentQueue<Log>();

        private object thisLock = new object();

        

        private Logger() { }
        private Logger(string logFileName, string path, string dateFormat, bool isEnable, bool toConsole)
        {
            LogFileName = logFileName;
            Path = path;
            DateFirmat = dateFormat;
            IsEnable = isEnable;
        }

        public static Logger GetInstance()
        {
            return GetInstance(logFileName: "log.txt", dateFormat: "MM/dd/yyyy hh:mm:ss tt K");
        }

        public static Logger GetInstance(string logFileName, string path = null, string dateFormat = null, bool isEnable = true, bool toConsole = false)
        {
            if (_instance == null)
                _instance = new Logger(logFileName, path, dateFormat, isEnable, toConsole);
            return _instance;
        }

        public void CreateLog(string message, StatusNum status)
        {
            Logs.Enqueue(new Log(message, status));
        }

        public void Start()
        {
            new Thread(SaveLog).Start();
        }

        private async void SaveLog()
        {
            while (IsEnable)
            {
                //lock (thisLock)
                {
                    if (Logs.Count > 0)
                    {
                        await ToFile(Logs);
                        await ClearListLog();
                    }
                }
                Thread.Sleep(500);
            }
        }
        
        private async Task ToFile(IEnumerable<Log> logs)
        {
            List<string> massiv = new List<string>();

            //lock (thisLock)
            {

                foreach (Log log in logs)
                    massiv.Add($"{log.Time.ToString(DateFirmat)} {log.Status.ToString()}: {log.Message}");

                await File.AppendAllLinesAsync(LogFileName, massiv);             

            }

        }

        private async Task ClearListLog()
        {
            await Task.Run(()=> Logs.Clear());
        }




    }
}
