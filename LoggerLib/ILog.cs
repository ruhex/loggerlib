﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoggerLib
{
    public interface ILog
    {
        public DateTimeOffset Time { get; set; }
        public string Message { get; set; }
        public StatusNum Status { get; set; }       
    }
}
