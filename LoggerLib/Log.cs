﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoggerLib
{
    internal class Log : ILog
    {
        // ISO 8601
        // DateTimeOffset.Now.ToString("o");

        public DateTimeOffset Time { get; set; }
        public string Message { get; set; }
        public StatusNum Status { get; set; }        

        public Log(string message, StatusNum status = StatusNum.INFO)
        {
            Status = status;
            Time = DateTimeOffset.Now;
            Message = message;
        }
    }
}
